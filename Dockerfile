# Use an official Node runtime as a parent image
FROM node:carbon

# Set the working directory to /app
WORKDIR '/app'

ADD http://www.random.org/strings/?num=10&len=8&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new uuid
RUN rm -rf /app
RUN cd /app && git clone https://Sam_Samy_Sameer@bitbucket.org/Sam_Samy_Sameer/nodehelloworld.git

# Copy package.json to the working directory
COPY package.json .

# Install any needed packages specified in package.json
RUN npm install

# Copying the rest of the code to the working directory
COPY . .

# Make port 3000 available to the world outside this container
EXPOSE 3000

# Run index.js when the container launches
CMD ["npm", "start"]
